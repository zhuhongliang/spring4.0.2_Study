package com.zhl.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class app {

	public static void main(String[] args){
		
		ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
		
		HelloWorld obj = (HelloWorld) context.getBean("a");
		
		//obj.setMsg("天王盖地虎  \t ");
		
		obj.printHelloWorld();
		
		obj.find();
	}
}
