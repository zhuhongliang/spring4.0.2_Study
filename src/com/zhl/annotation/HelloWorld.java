package com.zhl.annotation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

public class HelloWorld{

	private String msg;
	private String msgg;
	
	HelloWorld(){
		this.msgg = "宝塔镇河妖";
	}
	
	@Resource(name="b")
	public void setMsg(String msg){
		this.msg = msg;
	}
	
	
	@Resource(name="c")
	public void setMsgg(String msgg){
		this.msgg = msgg;
	}

	public void printHelloWorld() {
		
		System.out.println(msg + "\t" +msgg );
		
	}
	
	//下面是jdbc
	
	@Autowired
	private DataSource dataSource;
	
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}


	public void find() {
		Connection conn = null;
		
		try {
			conn = dataSource.getConnection();
			String sql = "select * from `user` ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println("----------------------------------");
				System.out.println("name\t"+rs.getString("name"));
				System.out.println("age\t"+rs.getInt("age"));
				System.out.println("sex\t"+rs.getString("sex"));
				

			}
	
		}catch(Exception e){
			System.out.println(e);
		}
	}
}
