package com.zhl.annotation;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SpringConfig {
	
	@Bean(name="dataSource")
    public DataSource dataSource() {  
        BasicDataSource dataSource = new BasicDataSource();  
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");  
        dataSource.setUrl("jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8");  
        dataSource.setUsername("root");  
        dataSource.setPassword("root");  
        return dataSource;  
    }  

	@Bean(name="a")
	public HelloWorld helloWorld(){
		HelloWorld a = new HelloWorld();
		return a;
	}
	
	
	@Bean(name="b")
	public String msggg(){
		String msggg = "小鸡炖蘑菇";
		return msggg;
	}
	
	
	@Bean(name="c")
	public String msgggg(){
		String msgggg = "一座玲珑塔，面向青寨背靠沙！";
		return msgggg;
	}
}
