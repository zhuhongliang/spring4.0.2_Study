package com.zhl.xml;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Spring bean
 * 
 */
public class HelloWorld {
	private String name;

	public void setName(String name) {
		this.name = name;
	}

	public void printHello() {
		System.out.println( name +" \t 手里提着五斤鳎蚂! ");
	}
	
	

	private DataSource dataSource;
	
	public void setDataSource(DataSource dataSource){
		this.dataSource = dataSource;
	}

	public void find() {
		Connection conn = null;
		
		try {
			conn = dataSource.getConnection();
			String sql = "select * from `user` ";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				
				System.out.println("----------------------------------");
				System.out.println("name\t"+rs.getString("name"));
				System.out.println("age\t"+rs.getInt("age"));
				System.out.println("sex\t"+rs.getString("sex"));

			}
	
		}catch(Exception e){
			System.out.println(e);
		}
	}
}