package com.zhl.xml;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class App {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml"); 
		
		HelloWorld obj = (HelloWorld) context.getBean("helloBean");
		
		//obj.setName("打南边来了一个喇嘛 \t ");
		obj.printHello();
		obj.find();
	}
	
}